<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>SnaFoo - Nerdery Snack Food Ordering System</title>
        <link href="${pageContext.request.contextPath}/css/modern.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
    <div class="masthead" role="banner">
        <div class="masthead-hd">
            <h1 class="hdg hdg_1 mix-hdg_extraBold"><a href="index.html">SnaFoo</a>
            </h1>
            <p class="masthead-hd-sub">Nerdery Snack Food Ordering System</p>
        </div>
        <div class="masthead-nav" role="navigation">
            <ul>
                <li><a href="index.html">Voting</a>
                </li>
                <li><a href="suggestions.html">Suggestions</a>
                </li>
                <li><a href="shoppinglist.html">Shopping List</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="wrapper">
        <div class="content" role="main">
            <div class="shelf shelf_5">
                <h2 class="hdg hdg_1">Suggestions</h2>
            </div>
            <div class="shelf shelf_2">
                <div class="error isHidden">You have attempted to add more than the allowed number of suggestions per month!
                    <br />There is a total of three allowed suggestions per month.</div>
                <div class="error isHidden">You have attempted to add a suggestion that already exists!</div>
                <div class="error isHidden">You have not completed information requested.</div>
            </div>
            <div class="content-centered">
                <div class="shelf shelf_2">
                    <form method="" action="" class="form" novalidate>
                        <fieldset class="shelf shelf_2">
                            <div class="shelf shelf_2">
                                <div class="shelf">
                                    <label for="snackOptions">
                                        <h2 class="hdg hdg_2">Select a snack from the list</h2>
                                    </label>
                                </div>
                                <select name="snackOptions" id="snackOptions">
                                    <option value="snack1">snack suggestion 1</option>
                                    <option value="snack2">snack suggestion 2</option>
                                    <option value="snack3">snack suggestion 3</option>
                                    <option value="snack4">snack suggestion 4</option>
                                </select>
                            </div>
                        </fieldset>
                        <div class="shelf shelf_5">
                            <p class="hdg hdg_1">or</p>
                        </div>
                        <fieldset class="shelf shelf_5">
                            <div class="shelf">
                                <label for="suggestionInput">
                                    <h2 class="hdg hdg_2">Enter new snack suggestion &amp; purchasing location</h2>
                                </label>
                            </div>
                            <div class="shelf">
                                <input type="text" id="suggestionInput" placeholder="Snack Suggestion" />
                            </div>
                            <div class="shelf">
                                <label for="suggestionLocation" class="isHidden">Location</label>
                                <input type="text" id="suggestionLocation" placeholder="Location" class="" />
                            </div>
                        </fieldset>
                        <input type="submit" value="Suggest this Snack!" class="btn">
                    </form>
                </div>
            </div>
        </div>
        <!-- /content -->
    </div>
    <!-- /wrapper -->
</body>
</html>
