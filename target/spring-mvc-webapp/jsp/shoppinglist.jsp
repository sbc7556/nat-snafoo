<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <title>SnaFoo - Nerdery Snack Food Ordering System</title>
        <link href="${pageContext.request.contextPath}/css/modern.css" rel="stylesheet">
        <link rel="shortcut icon" href="${pageContext.request.contextPath}/img/icon.png">
    </head>
    <body>
    <div class="masthead" role="banner">
        <div class="masthead-hd">
            <h1 class="hdg hdg_1 mix-hdg_extraBold"><a href="index.html">SnaFoo</a>
            </h1>
            <p class="masthead-hd-sub">Nerdery Snack Food Ordering System</p>
        </div>
        <div class="masthead-nav" role="navigation">
            <ul>
                <li><a href="index.html">Voting</a>
                </li>
                <li><a href="suggestions.html">Suggestions</a>
                </li>
                <li><a href="shoppinglist.html">Shopping List</a>
                </li>
            </ul>
        </div>
    </div>
    <div class="wrapper">
        <div class="content" role="main">
            <div class="shelf shelf_5">
                <h2 class="hdg hdg_1">Shopping List</h2>
            </div>
            <div class="shelf shelf_1">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">Snack Name</th>
                            <th scope="col">Purchase Location</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Donuts</td>
                            <td>Pies &amp; Cakes Bakery</td>
                        </tr>
                        <tr>
                            <td>Spam</td>
                            <td>Cub</td>
                        </tr>
                        <tr>
                            <td>Pistachios</td>
                            <td>Cub</td>
                        </tr>
                        <tr>
                            <td>Buckets of M&amp;M's</td>
                            <td>Cub</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
        <!-- /content -->
    </div>
    <!-- /wrapper -->
</body>
</html>
